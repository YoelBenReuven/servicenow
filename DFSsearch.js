var tree = {
  "1": {
    "2": {
      "7": null,
      "8": {
        "10": null
      },
      "9": null
    },
    "3": null,
    "4": null,
    "5": {
      "22": null
    },
    "6": null
  }
};

function DFSsearch(tree, search, str = []) {
  let keys = Object.keys(tree);

  if (keys) {
    for (const key of Object.keys(tree)) {
      if (key == search) {
        str.push(key);
        return str.toString(",");
      } else {
        str.push(key);
        if (tree[key] instanceof Object && tree[key] != "null") {
          let childsearch = DFSsearch(tree[key], search, str);
          if (childsearch !== undefined) {
            return childsearch;
          }
        }
      }
    }
  }
}

console.log(DFSsearch(tree, 22));
