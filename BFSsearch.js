var tree = {
  "1": {
    "2": {
      "7": null,
      "8": {
        "10": null
      },
      "9": null
    },
    "3": null,
    "4": null,
    "5": {
      "22": null
    },
    "6": null
  }
};

function BFSsearch(tree, num, str = []) {
  function search(tree, num, str, stack = {}) {
    for (const key of Object.keys(tree)) {
      if (key == num) {
        str.push(key);
        return str.toString(",");
      } else {
        str.push(key);

        if (tree[key] instanceof Object && tree[key] !== "null") {
          let val = tree[key];
          Object.assign(stack, val);
        }
      }
    }
    return search(stack, num, str);
  }
  return search(tree, num, str);
}

console.log(BFSsearch(tree, 10));
